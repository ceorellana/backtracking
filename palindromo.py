def esPalindromo(palabra, inicio, fin):
    while (inicio < fin):
        if (palabra[inicio] != palabra[fin]):
            return False
        inicio+=1
        fin-=1
    return True

def arrPalindormos(palabra,inicio,fin,arrParticiones,particion):
    if (inicio >= fin):
        arrParticiones.append(particion.copy())
        return
    for i in range (inicio,fin):
        if (esPalindromo(palabra, inicio, i)):
            particion.append(palabra[inicio:i + 1])
            arrPalindormos(palabra,i+1,fin,arrParticiones,particion)
            particion.pop()

palabra = input("Ingrese una palabra: ")
while (" " in palabra):
    palabra = input("Ingrese SOLO una palabra: ")
resultado = []
tmp = []
arrPalindormos(palabra,0,len(palabra),resultado,tmp)
print (resultado)