campo = \
[[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
[1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 1, 0, 1, 1, 1, 1, 1],
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
[1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
[0, 1, 1, 1, 1, 0, 1, 1, 1, 1],
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
[1, 1, 1, 0, 1, 1, 1, 1, 1, 1]]

def marcarZonasAnexas(i,j):
    if (i !=0):
        campo[i-1][j] = -1
    if (i !=len(campo)-1):
        campo[i+1][j] = -1
    if (j !=0):
        campo[i][j-1] = -1
    if (j !=len(campo[0])-1):
        campo[i][j+1] = -1

def agregarZonasPeligro():
    for i in range(len(campo)):
        for j in range (len(campo[0])):
            if (campo[i][j] == 0):
                marcarZonasAnexas(i,j)

def caminoMasCorto(anteriores,posI,posJ,caminos):
    if (posJ == len(campo[0])-1):
        caminos.append(len(anteriores)+1)
        return
    if (posI != len(campo)-1):
        if (posI!=0):
            if(campo[posI-1][posJ] == 1 and (posI-1,posJ) not in anteriores):
                nAnteriores = anteriores.copy()
                nAnteriores.append((posI, posJ))
                caminoMasCorto(nAnteriores, posI-1, posJ, caminos)
        if (campo[posI][posJ+1] == 1 and (posI,posJ+1) not in anteriores):
            nAnteriores = anteriores.copy()
            nAnteriores.append((posI, posJ))
            caminoMasCorto(anteriores.copy(),posI,posJ+1,caminos)
        if (campo[posI+1][posJ] == 1 and (posI+1,posJ) not in anteriores):
            nAnteriores = anteriores.copy()
            nAnteriores.append((posI, posJ))
            caminoMasCorto(anteriores.copy(), posI+1, posJ, caminos)
        if (posJ!=0):
            if (campo[posI][posJ-1] == 1 and (posI, posJ-1) not in anteriores):
                nAnteriores = anteriores.copy()
                nAnteriores.append((posI, posJ))
                caminoMasCorto(anteriores.copy(), posI, posJ-1, caminos)
    else:
        if (campo[posI-1][posJ] == 1 and (posI-1, posJ) not in anteriores):
            nAnteriores = anteriores.copy()
            nAnteriores.append((posI, posJ))
            caminoMasCorto(anteriores.copy(), posI-1, posJ, caminos)
        if (campo[posI][posJ+1] == 1 and (posI,posJ+1) not in anteriores):
            nAnteriores = anteriores.copy()
            nAnteriores.append((posI, posJ))
            caminoMasCorto(anteriores.copy(),posI,posJ+1,caminos)
        if (posJ!=0):
            if (campo[posI][posJ-1] == 1 and (posI, posJ-1) not in anteriores):
                nAnteriores = anteriores.copy()
                nAnteriores.append((posI, posJ))
                caminoMasCorto(anteriores.copy(), posI, posJ-1, caminos)

agregarZonasPeligro()
anteriores = []
caminos = []
for i in range(len(campo[0])):
    if (campo[i][0] == 1):
        caminoMasCorto(anteriores,i,0,caminos)
print(caminos)